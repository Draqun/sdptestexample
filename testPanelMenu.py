# vim:fileencoding=utf-8
# Copyright (c) GlobalLogic.

__version__ = "0.1"
__author__ = "Damian Giebas"

import unittest
import time
import random

from glsdp import GLBaseTestCase
from glsdp import GLSupportUI
from glsdp import GLWait
from glsdp import GLHelper

class PanelMenu(GLBaseTestCase, GLSupportUI):
    def glAfterSetUp(self):
        GLWait.whilePageLoaderIsVisible(self.driver)

    def testContactMenuLinkSlideAction(self):
        self.clickOnTopMenuLink(self.driver, "Contact")
        time.sleep(2)
        yValue = self.driver.execute_script("return window.scrollY")
        self.assertEqual(4197, yValue)

    def testRandomSubmenuElementLoadPage(self):
        topMenuTitles = GLHelper.getTopMenuTitles()
        topMenuTitle = topMenuTitles[random.randrange(len(topMenuTitles) - 1)]

        subMenuTitles = GLHelper.getSubMenuTitles(topMenuTitle)
        if not subMenuTitles:
            return

        subMenuTitle = subMenuTitles[random.randrange(len(subMenuTitles) - 1)]

        self.clickOnSubMenuLink(self.driver, topMenuTitle, subMenuTitle)
        time.sleep(1)
        GLWait.whilePageLoaderIsVisible(self.driver)
        pageHeader = self.findPageHeader(self.driver, subMenuTitle)
        self.assertIsNotNone(pageHeader)
        try:
            self.assertEqual(subMenuTitle.upper(), pageHeader.text)
        except AssertionError:
            self.assertEqual(subMenuTitle, pageHeader.text)

if __name__ == "__main__":
    unittest.main()
