# vim:fileencoding=utf-8
# Copyright (c) GlobalLogic.

__version__ = "0.1"
__author__ = "Damian Giebas"

import unittest
from selenium.webdriver.common.by import By

from glsdp import GLBaseTestCase
from glsdp import GLSupportUI
from glsdp import GLWait
from glsdp import GLHelper

class TestContactForm(GLBaseTestCase, GLSupportUI):
    driverType = GLHelper.PHANTOMJS
    baseUrl = "http://www.rec-global.com/#contact"

    def glAfterSetUp(self):
        GLWait.whilePageLoaderIsVisible(self.driver)
        self.contactForm = self.getFormUsingAttrs(self.driver,
            (
                {"attribute": "id", "value": "form"},
                {"attribute": "name", "value": "contactform"}
            ))

    def testNameFieldWhenFieldIsEmpty(self):
        self.clickOnInputTypeButtonWithText(self.contactForm, "SUBMIT")
        resultElement = GLWait.untilElementNotExist(self.contactForm, 2,
            By.XPATH, ".//div[@id='result']/div[@class='alert alert-danger']")
        self.assertEqual("Please Enter Name.", resultElement.text)

    def testEmailFieldWhenFieldIsEmptyOrHasInvalidValue(self):
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='name']", "TestName")
        self.clickOnInputTypeButtonWithText(self.contactForm, "SUBMIT")
        resultElement = GLWait.untilElementNotExist(self.contactForm, 2,
            By.XPATH, ".//div[@id='result']/div[@class='alert alert-danger']")
        self.assertEqual("Please Enter A Valid Email Address.",
            resultElement.text)

        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='email']", "ThisIsNotValidEMail")
        self.clickOnInputTypeButtonWithText(self.contactForm, "SUBMIT")
        resultElement = GLWait.untilElementNotExist(self.contactForm, 2,
            By.XPATH, ".//div[@id='result']/div[@class='alert alert-danger']")
        self.assertEqual("Please Enter A Valid Email Address.",
            resultElement.text)

    def testSubjectFieldWhenFieldIsEmpty(self):
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='name']", "TestName")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='email']", "test@globallogic.com")
        self.clickOnInputTypeButtonWithText(self.contactForm, "SUBMIT")
        resultElement = GLWait.untilElementNotExist(self.contactForm, 2,
            By.XPATH, ".//div[@id='result']/div[@class='alert alert-danger']")
        self.assertEqual("Please Enter Subject.", resultElement.text)

    def testMessageFieldWhenFieldIsEmpty(self):
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='name']", "TestName")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='email']", "test@globallogic.com")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='subject']", "TestSubject")
        self.clickOnInputTypeButtonWithText(self.contactForm, "SUBMIT")
        resultElement = GLWait.untilElementNotExist(self.contactForm, 2,
            By.XPATH, ".//div[@id='result']/div[@class='alert alert-danger']")
        self.assertEqual("Please Write More Than 10 Characters In Message",
            resultElement.text)

    def testMessageFieldWhenFieldHas9Characters(self):
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='name']", "TestName")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='email']", "test@globallogic.com")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='subject']", "TestSubject")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//textarea[@name='msg']", "TestMsg89")
        self.clickOnInputTypeButtonWithText(self.contactForm, "SUBMIT")
        resultElement = GLWait.untilElementNotExist(self.contactForm, 2,
            By.XPATH, ".//div[@id='result']/div[@class='alert alert-danger']")
        self.assertEqual("Please Write More Than 10 Characters In Message",
            resultElement.text)

    def testCapchaFieldWhenFieldIsEmpty(self):
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='name']", "TestName")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='email']", "test@globallogic.com")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='subject']", "TestSubject")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//textarea[@name='msg']", "This is test message. Ignore it.")
        self.clickOnInputTypeButtonWithText(self.contactForm, "SUBMIT")
        resultElement = GLWait.untilElementNotExist(self.contactForm, 2,
            By.XPATH, ".//div[@id='result']/div[@class='alert alert-danger']")
        self.assertEqual("Please Enter Correct Number, Try Again!",
            resultElement.text)

    def testCapchaFieldHasInvalidValue(self):
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='name']", "TestName")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='email']", "test@globallogic.com")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='subject']", "TestSubject")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//textarea[@name='msg']", "This is test message. Ignore it.")
        self.findElementAndPutText(self.contactForm, By.XPATH,
            ".//input[@name='human_test']", "27")
        self.clickOnInputTypeButtonWithText(self.contactForm, "SUBMIT")
        resultElement = GLWait.untilElementNotExist(self.contactForm, 2,
            By.XPATH, ".//div[@id='result']/div[@class='alert alert-danger']")
        self.assertEqual("Please Enter Correct Number, Try Again!",
            resultElement.text)

if __name__ == "__main__":
    unittest.main()
